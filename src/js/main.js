/*
ON LOAD PAGE FUNCTION
*/

jQuery( window ).on( 'load', function() {

    $('body').removeClass('is-load');

} );

/*
INITIALIZATION FUNCTIONS
*/

jQuery( document ).ready( function( $ ) {

    initSliders();
    toggleMobileNav();

} );

/*
ON SCROLL PAGE FUNCTIONS
*/

jQuery( window ).on( 'scroll', function() {



} );

function initSliders() {
    let mainSlider = $('.js-main-slider');

    if ( mainSlider.length ) {

        mainSlider.slick( {

        } );

    }
}

function toggleMobileNav() {
    let toggleMobileNav = $('.js-button-nav'),
        mobileNav = $('.js-mobile-nav');

    if ( mobileNav.length ) {
        toggleMobileNav.on( 'click', function( e ) {
            $(this).toggleClass('is-active');
            mobileNav.toggleClass('is-active');

            if ( mobileNav.hasClass('is-active') ) {
                scrollLock.hide();
            } else {
                scrollLock.show();
            }

            e.preventDefault();
        } );
    }

}